package lesson_1;

public class Calculator {
    public double add(double firstNumber, double secondNumber) {
        return firstNumber + secondNumber;
    }

    public double sub(double firstNumber, double secondNumber) {
        return firstNumber - secondNumber;
    }

    public double mult(double firstNumber, double secondNumber) {
        return firstNumber * secondNumber;
    }

    public double div(double firstNumber, double secondNumber) {
        return firstNumber / secondNumber;
    }
}
