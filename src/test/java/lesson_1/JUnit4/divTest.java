package lesson_1.JUnit4;

import lesson_1.Calculator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import  org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;
@Category(CalcSuit.class)
public class divTest {
    static Calculator calculator;
    @Before
    public void start(){
        calculator=new Calculator();
        System.out.println("Start test div");
    }
    @Test
    public void div() throws Exception {
        Double result=calculator.div(6,3);
        Assert.assertEquals( 2 ,result,2/1e3);
    }

    @Test
    public void div_by_negative() throws Exception{
        Double result=calculator.div(33,-3);
        Assert.assertEquals( -11.0 ,result,11/1e3);
    }
    @Test
    public void div_negative_by_negative() throws Exception{
        Double result=calculator.div(-33,-3);
        Assert.assertEquals( 11.0 ,result,11/1e3);
    }
    @Test
    public void div_negative() throws Exception{

        Double result=calculator.div(-33,3);
        Assert.assertEquals( -11,result,11/1e3);
    }

    @Test
    public void div_by_zero() throws Exception{
        Double result=calculator.div(11,0);
        Assert.assertEquals( true ,result.isInfinite()||result.isNaN());
    }
    @Test
    public void div_zero() throws Exception{

        Double result=calculator.div(0,11);
        Assert.assertEquals( 0,result,0/1e3);
    }
    @Test
    public void div_zero_by_zero() throws Exception{

        Double result=calculator.div(0,0);
        Assert.assertEquals( true ,result.isInfinite()||result.isNaN());
    }
    @Test
    public void div_by_one() throws Exception{
        Double result=calculator.div(11,0);
        Assert.assertEquals( true ,result.isInfinite()||result.isNaN());
    }
    @Test
    public void div_fractional_numbers() throws Exception{
        Double result=calculator.div(1.2,0.3);
        Assert.assertEquals( 4 ,result,4/1e32);
    }
    @Test
    public void div_by_fractional_number() throws Exception{
        Double result=calculator.div(3,0.3);
        Assert.assertEquals( 10 ,result,10/1e4);
    }
    @Test
    public void div__fractional_numbers() throws Exception{
        Double result=calculator.div(1,3);
        Assert.assertEquals( (1.0/3) ,result,1/3/1e3);
    }
    @Test(expected = NullPointerException.class)
    public void add_2_null() throws Exception {
        calculator.div((Double)null,3);
    }
    @Test(expected = NullPointerException.class)
    public void add_null() throws Exception {
        calculator.div((Double)null,(Double)null);
    }
    @After
    public void end(){
        calculator=null;
    }

}
