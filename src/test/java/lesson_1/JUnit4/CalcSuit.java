package lesson_1.JUnit4;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Categories.IncludeCategory(CalcSuit.class)
@Suite.SuiteClasses({addTest.class,divTest.class,minTest.class,multTest.class})
public class CalcSuit {

}
