package lesson_1.JUnit4;
import lesson_1.Calculator;
import org.junit.Assert;
import  org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
@Category(CalcSuit.class)
public class multTest {
    private Calculator calculator;

    private int a;
    private int b;
    private int result;



    public multTest(int a, int b, int result) {
        this.a = a;
        this.b = b;
        this.result = result;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> getTestData() {
        return Arrays.asList(new Object[][]{
                {2,2,4},
                {1,2,2},
                {0,2,0},
                {2,0,0}

        });
    }



    @Test
    public void mult() throws Exception {
        calculator=new Calculator();
        Assert.assertTrue("\nисходные данные\n"+"a= "+a+"\n"+"b= "+b+"\n"+"result= "+result+"\n",calculator.mult(a,b)==result);
    }
    @Test(expected = NullPointerException.class)
    public void add_2_null() throws Exception {
        calculator.sub((Double)null,3);
    }
    @Test(expected = NullPointerException.class)
    public void add_null() throws Exception {
        calculator.sub((Double)null,(Double)null);
    }
}

