package lesson_1.JUnit4;
import lesson_1.Calculator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import  org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(CalcSuit.class)
public class minTest {
    private static Calculator calculator;
    @Before
    public void  start(){
        calculator=new Calculator();
    }
    @Test
    public void min() throws Exception {
        Assert.assertEquals(0,calculator.sub(1,1),1/1e4);
    }
    @Test(expected = NullPointerException.class)
    public void add_2_null() throws Exception {
        calculator.sub((Double)null,3);
    }
    @Test(expected = NullPointerException.class)
    public void add_null() throws Exception {
        calculator.sub((Double)null,(Double)null);
    }
    @Test
    public void min_with_1_negative_param() throws Exception {
        Assert.assertEquals(2,calculator.sub(1,-1),1/1e3);
    }
    @Test
    public void min_with_all_negative_param() throws Exception {
        Assert.assertEquals(0,calculator.sub(-1,-1),1/1e4);
    }
    @Test
    public void min_wit_with_2_zero() throws Exception {
        Assert.assertEquals(0,calculator.sub(0,0),1/1e4);
    }
    @Test
    public void min_wit_with_1_zero() throws Exception {
        Assert.assertEquals(5,calculator.sub(5,0),5/1e3);
    }
    @After
    public void end(){
        calculator=null;
    }
}
