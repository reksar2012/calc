package lesson_1.TestNG;

import lesson_1.Calculator;
import org.junit.After;
import org.junit.Before;
import org.testng.annotations.*;
import sun.util.locale.LocaleUtils;

import java.util.Locale;

import static org.testng.Assert.*;

public class CalculatorTest {
    private static Calculator calculator;

    @DataProvider()
    public Object[][] AddData() {
        return new Object[][]{
                {1.0,1.0,2.0},
                {1.0,-2.0,-1.0},
                {1.5,-2.0,-0.5},
                {0.0,1.0,1.0},
                {-1.0,-1.0,-2.0}
        };
    }


    @Test(dataProvider = "AddData", groups={"Positive","ADD"})
    public void testAdd(Double firstNumber,Double secondNumber,Double result) throws Exception {
        calculator=new Calculator();
    assertEquals(result,calculator.add(firstNumber,secondNumber));
    }



    @DataProvider()
    public Object[][] SubData() {
        return new Object[][]{
                {1.0,1.0,0.0},
                {1.0,-2.0,3.0},
                {1.5,-2.0,3.5},
                {1.5,-2.1,3.6},
                {0.0,1.0,-1.0},
                {-1.0,-1.0,0.0}
        };
    }

    @Test(dataProvider = "SubData", groups="Positive")
    public void testSub(Double firstNumber,Double secondNumber,Double result) throws Exception {
        calculator=new Calculator();
        assertEquals(result, calculator.sub(firstNumber, secondNumber));
    }

    @DataProvider()
    public Object[][] MultData() {
        return new Object[][]{
                {1.0,1.0,1.0},
                {1.0,-2.0,-2.0},
                {-1.5,2.8,-4.2},
                {0.0,1.0,0.0},
                {3.0,1.0,3.0}
        };
    }

    @Test(dataProvider = "MultData", groups="Positive")
    public void testMult(Double firstNumber,Double secondNumber,Double result) throws Exception {
        calculator=new Calculator();
        assertEquals(String.format("%.1f",result), String.format("%.1f",calculator.mult(firstNumber, secondNumber)));
    }

    @DataProvider()
    public Object[][] DivData() {
        return new Object[][]{
                {1.0,1.0,1.0},
                {1.0,-2.0,-0.5},
                {-0.246,0.82,-0.3},
                {0.0,1.0,0.0},
                {3.0,1.0,3.0}
        };
    }

    @Test(dataProvider = "DivData", groups="Positive")
    public void testDiv(Double firstNumber,Double secondNumber,Double result) throws Exception {
        calculator=new Calculator();
        assertEquals(String.format("%.1f",result), String.format("%.1f",calculator.div(firstNumber, secondNumber)));
    }

}